angular.module('apiclient', []);

angular
    .module('apiclient')
    .factory('ApiClientFactory', ApiClientFactory)
    .constant('ApiRootUrl', 'http://api.snowcrash.com');
