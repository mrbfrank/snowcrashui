/**
 * Simple $http service that provides url creation for our api
 */
function ApiClientFactory($http, ApiRootUrl) {
    var api = {};
    /**
     * Makes a request object that we use to make our api requests
     * @param {string} method - method of request
     * @param {string} url - url path of request
     * @param {object} data - data object to pass into the request
     * @param {object} headers - collectin of headers to use
     */
    api.makeRequest = function(method, url, data, headers) {
        var req = {
            method: method,
            url: ApiRootUrl + url,
            headers: headers
        };

        if (method === 'GET') {
            req.params = data;
        } else {
            req.data = data;
        }

        return req;
    };

    /**
     * GET request
     */
    api.GET = function(url, data, headers) {
        var req = this.makeRequest('GET', url, data, headers);
        return $http(req);
    };

    /**
     * POST request
     */
    api.POST = function(url, data, headers) {
        var req = this.makeRequest('POST', url, data, headers);
        return $http(req);
    };

    /**
     * DELETE request
     */
    api.DELETE = function(url, data, headers) {
        var req = this.makeRequest('DELETE', url, data, headers);
        return $http(req);
    };

    return api;
}
