/**
 * Factory that returns api response objects from /settings resource
 */
function SettingsFactory(ApiClientFactory, MockDataFactory) {
    var game = {};

    // Settings
    game.getSettings = function(params) {
        return ApiClientFactory.GET('/settings', params)
            .then(function successCallback(response) {
                // console.log('GET /settings response', response);
                return response.data;
            }, function errorCallback(response) {
                console.log('error', response);
            });
    };

    game.getSetting = function(id) {
        return ApiClientFactory.GET('/settings/' + id)
            .then(function successCallback(response) {
                // console.log('GET /settings/:id response', response);
                return response.data;
            }, function errorCallback(response) {
                console.log('error', response);
            });
    };

    game.createSetting = function(setting) {
        return ApiClientFactory.POST('/settings', setting)
            .then(function successCallback(response) {
                // console.log('POST /settings response', response);
                return response.data;
            }, function errorCallback(response) {
                console.log('error', response);
            });
    };

    game.updateSetting = function(setting) {
        return ApiClientFactory.POST('/settings/' + setting.id, setting)
            .then(function successCallback(response) {
                // console.log('POST /settings/:id response', response);
                return response.data;
            }, function errorCallback(response) {
                console.log('error', response);
            });
    };

    game.deleteSetting = function(setting) {
        return ApiClientFactory.DELETE('/settings/' + setting.id, setting)
            .then(function successCallback(response) {
                // console.log('DELETE /settings/:id response', response);
                return response.data;
            }, function errorCallback(response) {
                console.log('error', response);
            });
    };


    // Groups
    game.getGroups = function() {
        return ApiClientFactory.GET('/groups')
            .then(function successCallback(response) {
                // console.log('GET /groups response', response);
                return response.data;
            }, function errorCallback(response) {
                console.log('error', response);
            });
    };

    game.getGroup = function(id) {
        return ApiClientFactory.GET('/groups/' + id)
            .then(function successCallback(response) {
                // console.log('GET /groups/:id response', response);
                return response.data;
            }, function errorCallback(response) {
                console.log('error', response);
            });
    };

    game.createGroup = function(group) {
        return ApiClientFactory.POST('/groups', group)
            .then(function successCallback(response) {
                // console.log('POST /groups response', response);
                return response.data;
            }, function errorCallback(response) {
                console.log('error', response);
            });
    };

    game.updateGroup = function(group) {
        return ApiClientFactory.POST('/groups/' + group.id, group)
            .then(function successCallback(response) {
                // console.log('POST /groups/:id response', response);
                return response.data;
            }, function errorCallback(response) {
                console.log('error', response);
            });
    };

    game.deleteGroup = function(group) {
        return ApiClientFactory.DELETE('/groups/' + group.id, group)
            .then(function successCallback(response) {
                // console.log('DELETE /groups/:id response', response);
                updateOrphans(group);
                return response.data;
            }, function errorCallback(response) {
                console.log('error', response);
            });
    };

    // Helper method to assign orphaned deleted-group children to deleted-group parent
    updateOrphans = function (deleted) {
        // Find all children groups of deleted-group.
        var groups = MockDataFactory.findAll('groups');
        for (var i = 0, len = groups.length; i < len; i++) {
            if (groups[i].parent === deleted.id) {
                // Assign deleted-group parent to child
                groups[i].parent = deleted.parent;
            }
        }

        // Find all children settings of deleted-group.
        var settings = MockDataFactory.findAll('settings');
        for (i = 0, len = settings.length; i < len; i++) {
            if (settings[i].group === deleted.id) {
                // Assign deleted-group parent to child
                settings[i].group = deleted.parent;
            }
        }
    };

    return game;
}
