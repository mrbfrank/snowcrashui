function SettingsCtrl (
    $scope,
    $rootScope,
    $location,
    $anchorScroll,
    SettingsFactory,
    StorageFactory
) {

    // // Retrieve settings from LocalStorage or API
    // if (!StorageFactory.getItem('settings')) {
    //     // GET & store settings
    //     console.log('GET settings from api');
    //     SettingsFactory.getSettings()
    //         .then(function (data) {
    //             StorageFactory.setItem('settings', data);
    //             $scope.settings = StorageFactory.getItem('settings');
    //         });
    // } else {
    //     // Load settings from LocalStorage
    //     console.log('Load settings from localstorage');
    //     $scope.settings = StorageFactory.getItem('settings');
    // }

    // Initialize scope variables
    $scope.typeOptions = ['string', 'number', 'boolean', 'null'];
    $scope.groupFilter = {};
    $scope.groupOptionsFilter = {};

    // Add/Edit a Setting
    $scope.createSetting = function(setting) {
        // console.log('addSetting', setting);
        // Add KVP to settings object
        if (setting.key !== undefined) {
            SettingsFactory.createSetting(setting)
            .then(function (data) {
                console.log('postSetting', data);
                // $scope.settings.push(data);
                getSettings();
                $scope.initEditSetting();
            });
        }
    };

    // Update a Setting
    $scope.updateSetting = function(setting) {
        SettingsFactory.updateSetting(setting)
            .then(function (data) {
                // console.log('updateSetting', data);
                getSettings();
                $scope.initEditSetting();
            });
    };

    // Delete a Setting
    $scope.deleteSetting = function(setting) {
        // console.log(setting);
        SettingsFactory.deleteSetting(setting)
            .then(function (data) {
                // console.log('deleteSetting', data);
                getSettings();
                $scope.initEditSetting();
            });
    };

    $scope.selectSetting = function(setting) {
        // console.log('selectedSetting', setting);
        $scope.setting = angular.copy(setting);
        $scope.selectedSetting = setting;

        // Initialize 'Edit Setting' Type
        if ($scope.setting.value === null) {
            $scope.setting.type = 'null';
        } else {
            $scope.setting.type = typeof setting.value;
        }

        // Cancel group edit
        $scope.initEditGroup();
    };

    $scope.goToEditSetting = function() {
        $location.hash('editsetting');
        $anchorScroll();
    };

    $scope.goToEditGroup = function() {
        $location.hash('editgroup');
        $anchorScroll();
    };

    $scope.initEditSetting = function() {
        $scope.selectedSetting = undefined;
        $scope.setting = {};
        $scope.setting.type = 'string';
        $scope.setting.group = 1;
    };

    $scope.clearFilter = function() {
        $scope.groupFilter = {};
    };

    // Cast Values to selected Type
    $scope.changeType = function() {
        if ($scope.setting.type === 'number') {
            $scope.setting.value = Number($scope.setting.value);
        }
        if ($scope.setting.type === 'string') {
            $scope.setting.value = String($scope.setting.value);
        }
        if ($scope.setting.type === 'boolean') {
            if ($scope.setting.value === 'true'){
                $scope.setting.value = true;
            } else {
                $scope.setting.value = false;
            }
        }
        if ($scope.setting.type === 'null') {
            $scope.setting.value = null;
        }
    };

    // - GROUP -
    // Group Tree START
    $scope.filterGroup = function(data) {
        $scope.groupFilter['group'] = data.id;
    };

    $scope.deleteNode = function(data) {
        data.nodes = [];
    };
    $scope.toggleNode = function(data) {
        data.show = !data.show;
    };
    $scope.addNode = function(data) {
        var post = data.nodes.length + 1;
        var newName = data.name + '-' + post;
        data.nodes.push({name: newName, show: true, nodes: []});
    };

    function arrayToTree(arr, parentID, childID) {
        var tree = [],
            mappedArr = {},
            arrElem,
            mappedElem;

        // First map the nodes of the array to an object -> create a hash table.
        for(var i = 0, len = arr.length; i < len; i++) {
            arrElem = arr[i];
            mappedArr[arrElem.id] = arrElem;
            mappedArr[arrElem.id][childID] = [];
        }

        for (var id in mappedArr) {
            if (mappedArr.hasOwnProperty(id)) {
                mappedElem = mappedArr[id];
                // If the element is not at the root level, add it to its parent array of children.
                if (mappedElem[parentID]) {
                    mappedArr[mappedElem[parentID]][childID].push(mappedElem);
                }
                // If the element is at the root level, add it to first level elements array.
                else {
                    tree.push(mappedElem);
                }
            }
        }
        return tree;
    }

    function getTree() {
        var arr = angular.copy($scope.groups);

        // Add {"show":true} to all objects in the array
        arr.map(function(obj) {
            return angular.extend(obj, {"show":true});
        });

        return arrayToTree(arr, 'parent', 'nodes');
    }
    // Group Tree END

    $scope.selectGroup = function(group) {
        // console.log('selectedGroup', group);
        $scope.group = angular.copy(group);
        $scope.selectedGroup = group;
        $scope.filterGroup(group);

        // Filter group options, prevents selected group from assigning self as parent
        $scope.groupOptionsFilter['name'] = '!'+group.name;

        // Cancel setting edit
        $scope.initEditSetting();
    };

    $scope.initEditGroup = function() {
        $scope.selectedGroup = undefined;
        $scope.groupOptionsFilter = {};
        $scope.group = {};
        $scope.group.parent = 1;
    };

    // Add/Edit a Group
    $scope.createGroup = function(group) {
        // console.log('createGroup', group);
        // Add KVP to settings object
        if (group.name !== undefined) {
            SettingsFactory.createGroup(group)
            .then(function (data) {
                // console.log('createGroup response', data);
                getGroups();
                $scope.initEditGroup();
            });
        }
    };

    // Update a Group
    $scope.updateGroup = function(group) {
        SettingsFactory.updateGroup(group)
            .then(function (data) {
                // console.log('updateGroup', data);
                getGroups();
                $scope.initEditGroup();
            });
    };

    // Delete a Setting
    $scope.deleteGroup = function(group) {
        // console.log('deleteGroup', group);
        SettingsFactory.deleteGroup(group)
            .then(function (data) {
                // console.log('deleteGroup', data);
                getSettings();
                getGroups();
                $scope.initEditGroup();
                $scope.clearFilter();
            });
    };

    // Build object for Settings UI
    buildSettingsUI = function(settings) {
        $scope.settingsUI = {};
        // Flatten settings to key:value pairs
        for (var i = 0, len = settings.length; i < len; i++) {
            $scope.settingsUI[settings[i].key] = settings[i].value;
        }
        // console.log('settingsUI', $scope.settingsUI);
    };

    // Retrieve settings from API (via SettingsFactory)
    getSettings = function() {
        SettingsFactory.getSettings()
            .then(function (data) {
                $scope.settings = data;
                buildSettingsUI(data);
            });
    };

    getGroups = function() {
        SettingsFactory.getGroups()
            .then(function (data) {
                $scope.groups = data;
                $scope.tree = getTree();
            });
    };

    // Initialize settings list
    getSettings();
    getGroups();

    // Initialize Edit Setting/Group input fields
    $scope.initEditSetting();
    $scope.initEditGroup();


    // Test GET Settings by Group query
    // getSettingsGroup = function(groupID) {
    //     var params = {};
    //     params.group = groupID;
    //     SettingsFactory.getSettings(params)
    //         .then(function (data) {
    //             $scope.settingsGroup = data;
    //             console.log('$scope.settingsGroup', $scope.settingsGroup);
    //         });
    // };
    // getSettingsGroup(3);

}
