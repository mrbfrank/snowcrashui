/**
 * Factory that provides a mock server data model
 */

function MockDataFactory($http) {
    var mock = {};

    // Load mockdata.json from local assets
    $http.get('assets/mockdata.json').success(function(data) {
        mock.data = data;
        // console.log('mock.data =', mock.data);
    });

    mock.getData = function(collection) {
        return mock.data[collection];
    };

    mock.setData = function(data) {
        mock.data = data;
    };

    mock.findOne = function(collection, id) {
        // find the resource that matches that id
        var list = $.grep(mock.getData(collection), function(element, index) {
            return (element.id == id);
        });
        if(list.length === 0) {
            return {};
        }
        // even if list contains multiple items, just return first one
        return list[0];
    };

    mock.findAll = function(collection) {
        return mock.getData(collection);
    };

    // options parameter is an object with key value pairs
    // in this simple implementation, value is limited to a single value (no arrays)
    mock.findMany = function(collection, options) {
        // find resource that match all of the options
        var list = $.grep(mock.getData(collection), function(element, index) {
            var matchAll = true;
            $.each(options, function(optionKey, optionValue) {
                if(element[optionKey] != optionValue) {
                    matchAll = false;
                    return false;
                }
            });
            return matchAll;
        });
        return list;
    };

    // add a new data item that does not exist already
    // must compute a new unique id and backfill in
    mock.addOne = function(collection, dataItem) {
        // must calculate a unique ID to add the new data
        var newId = mock.newId(collection);
        dataItem.id = newId;
        mock.data[collection].push(dataItem);
        return dataItem;
    };

    // return an id to insert a new data item at
    mock.newId = function(collection) {
        // find all current ids
        var currentIds = $.map(mock.getData(collection), function(dataItem) { return dataItem.id; });
        // since id is numeric, and we will treat like an autoincrement field, find max
        var maxId = Math.max.apply(Math, currentIds);
        // increment by one
        return maxId + 1;
    };

    mock.updateOne = function(collection, id, dataItem) {
        // find the resource that matches that id
        var mockData = mock.getData(collection);
        var match = null;
        for (var i=0; i < mockData.length; i++) {
            if(mockData[i].id == id) {
                match = mockData[i];
                break;
            }
        }
        if(!angular.isObject(match)) {
            return {};
        }
        angular.extend(match, dataItem);
        return match;
    };

    mock.deleteOne = function(collection, id) {
        // find the resource that matches that id
        var mockData = mock.getData(collection);
        var match = false;
        for (var i=0; i < mockData.length; i++) {
            if(mockData[i].id == id) {
                match = true;
                mockData.splice(i, 1);
                break;
            }
        }
        return match;
    };

    return mock;
}
