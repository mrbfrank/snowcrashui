angular.module('mockserver', ['ngMockE2E']);

function appRun (
    $rootScope,
    $httpBackend,
    MockDataFactory,
    ApiRootUrl
) {

    // Initialize Foundation
    $rootScope.$on('$viewContentLoaded', function () {
        $(document).foundation();
    });

    // - HTTP Mocks -
    // Allow loading mockdata.json from local assets
    $httpBackend.whenGET('assets/mockdata.json').passThrough();


    // - Settings -
    // GET /settings
    $httpBackend.whenGET(ApiRootUrl+'/settings').respond(function(method, url, data) {
        var settings = MockDataFactory.findAll('settings');
        return [200, settings, {}];
    });

    // GET /settings/:id
    $httpBackend.whenGET(/\/settings\/\d+/).respond(function(method, url, data) {
        // parse the matching URL to pull out the id (/settings/:id)
        var settingid = url.split('/')[4];
        var setting = MockDataFactory.findOne('settings', settingid);
        return [200, setting, {}];
    });

    // POST /settings
    // Create a new resource
    $httpBackend.whenPOST(ApiRootUrl+'/settings').respond(function(method, url, data) {
        var params = angular.fromJson(data);
        var setting = MockDataFactory.addOne('settings', params);
        // get the id of the new resource to populate the Location field
        var settingid = setting.id;
        return [201, setting, { Location: '/settings/' + settingid }];
    });

    // POST /settings/:id
    // Update an existing resource (ngResource does not send PUT for update)
    $httpBackend.whenPOST(/\/settings\/\d+/).respond(function(method, url, data) {
        var params = angular.fromJson(data);
        // parse the matching URL to pull out the id (/settings/:id)
        var settingid = url.split('/')[4];
        var setting = MockDataFactory.updateOne('settings', settingid, params);
        return [201, setting, { Location: '/settings/' + settingid }];
    });

    // DELETE /settings/:id
    $httpBackend.whenDELETE(/\/settings\/\d+/).respond(function(method, url, data) {
        // parse the matching URL to pull out the id (/settings/:id)
        var settingid = url.split('/')[4];
        MockDataFactory.deleteOne('settings', settingid);
        return [204, {}, {}];
    });

    // GET /settings?group=:id
    $httpBackend.whenGET(/\/settings\?group=\d+/).respond(function(method, url, data) {
        // parse the matching URL to pull out the id (/settings?group=:id)
        var groupid = url.split('=')[1];
        var params = {};
        params.group = Number(groupid);
        settings = MockDataFactory.findMany('settings', params);
        return [200, settings, {}];
    });


    // - Groups --
    // GET /groups
    $httpBackend.whenGET(ApiRootUrl+'/groups').respond(function(method, url, data) {
        var groups = MockDataFactory.findAll('groups');
        return [200, groups, {}];
    });

    // GET /groups/:id
    $httpBackend.whenGET(/\/groups\/\d+/).respond(function(method, url, data) {
        // parse the matching URL to pull out the id (/groups/:id)
        var groupid = url.split('/')[4];
        var group = MockDataFactory.findOne('groups', groupid);
        return [200, group, {}];
    });

    // POST /groups
    // Create a new resource
    $httpBackend.whenPOST(ApiRootUrl+'/groups').respond(function(method, url, data) {
        var params = angular.fromJson(data);
        var group = MockDataFactory.addOne('groups', params);
        // get the id of the new resource to populate the Location field
        var groupid = group.id;
        return [201, group, { Location: '/groups/' + groupid }];
    });

    // POST /groups/:id
    // Update an existing resource (ngResource does not send PUT for update)
    $httpBackend.whenPOST(/\/groups\/\d+/).respond(function(method, url, data) {
        var params = angular.fromJson(data);
        // parse the matching URL to pull out the id (/groups/:id)
        var groupid = url.split('/')[4];
        var group = MockDataFactory.updateOne('groups', groupid, params);
        return [201, group, { Location: '/groups/' + groupid }];
    });

    // DELETE /groups/:id
    $httpBackend.whenDELETE(/\/groups\/\d+/).respond(function(method, url, data) {
        // parse the matching URL to pull out the id (/groups/:id)
        var groupid = url.split('/')[4];
        MockDataFactory.deleteOne('groups', groupid);
        return [204, {}, {}];
    });

}

angular
    .module('mockserver')
    .factory('MockDataFactory', MockDataFactory)
    .run(appRun);
