var modules = [
    // 3rd party
    'ui.router',
    'templates',
    'LocalStorageModule',

    // components
    'settings',
    'storage',
    'mockserver'
];

angular.module('SnowCrash', modules);

function appConfig(
    $stateProvider,
    $urlRouterProvider,
    $httpProvider
) {

    // For any unmatched url, redirect to index
    $urlRouterProvider.otherwise('settings/edit');

    $stateProvider
        .state('main', {
            // url: 'main',
            abstract: true,
            views: {
                'topnav': {
                    templateUrl: 'layout/topnav.html'
                },
                'body': {
                    templateUrl: 'layout/body.html'
                },
                'footer': {
                    templateUrl: 'layout/footer.html'
                }
            }
        })

        .state('metaverse', {
            parent: 'main',
            url: "/metaverse",
            views: {
                'content@main': {
                    templateUrl: 'layout/metaverse.html'
                }
            }
        })

        .state('settings', {
            parent: 'main',
            url: "/settings",
            views: {
                'content@main': {
                    templateUrl: 'settings/settings.html',
                    controller: 'SettingsCtrl'
                }
            }
        })

        .state('video', {
            parent: 'main',
            url: "/settings/video",
            views: {
                'content@main': {
                    templateUrl: 'settings/partials/video.html',
                    controller: 'SettingsCtrl'
                }
            }
        })

        .state('audio', {
            parent: 'main',
            url: "/settings/audio",
            views: {
                'content@main': {
                    templateUrl: 'settings/partials/audio.html',
                    controller: 'SettingsCtrl'
                }
            }
        })

        .state('gameplay', {
            parent: 'main',
            url: "/settings/gameplay",
            views: {
                'content@main': {
                    templateUrl: 'settings/partials/gameplay.html',
                    controller: 'SettingsCtrl'
                }
            }
        })

        .state('controls', {
            parent: 'main',
            url: "/settings/controls",
            views: {
                'content@main': {
                    templateUrl: 'settings/partials/controls.html',
                    controller: 'SettingsCtrl'
                }
            }
        })

        .state('edit', {
            parent: 'main',
            url: "/settings/edit",
            views: {
                'content@main': {
                    templateUrl: 'settings/edit.html',
                    controller: 'SettingsCtrl'
                }
            }
        });

        // Intercept API calls and do cool stuff
        $httpProvider.interceptors.push(function($q, $rootScope) {
            $rootScope.apilog = '';
            var logme;
            return {
                'request': function(config) {
                    // Echo API request to background
                    logme = JSON.stringify(config, null, 2);
                    $rootScope.apilog = $rootScope.apilog + logme;

                    // Filter templates from console
                    if (config.url.indexOf('.html') === -1) {
                        console.log('Request: '
                            + config.method + ' '
                            + config.url, config);
                    };
                    return config;
                },

                'response': function(response) {
                    // Echo API response to background
                    logme = JSON.stringify(response, null, 2);
                    $rootScope.apilog = $rootScope.apilog + logme;

                    // Filter templates from console
                    if (response.config.url.indexOf('.html') === -1) {
                        console.log('Response: '
                            + response.config.method + ' '
                            + response.config.url, response);
                    };
                    return response;
                }
            };
        });
}

function appRun (
    $rootScope
) {
    // Initialize Foundation
    $rootScope.$on('$viewContentLoaded', function () {
        $(document).foundation();
    });
}

angular
    .module('SnowCrash')
    .config(appConfig)
    .run(appRun);
