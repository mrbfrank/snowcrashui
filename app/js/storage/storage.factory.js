function StorageFactory($window, localStorageService) {
    var storage = {};

    storage.setItem = function(key, value) {
        return localStorageService.set(key, value);
    };

    storage.getItem = function(key) {
        return localStorageService.get(key);
    };

    storage.deleteItem = function(key) {
        return localStorageService.remove(key);
    };

    storage.setObject = function(key, obj) {
        var objstr = angular.toJson(obj);
        return localStorageService.set(key, objstr);
    };

    storage.getObject = function(key) {
        return angular.fromJson(localStorageService.get(key));
    };

    storage.clearAll = function() {
        return localStorageService.clearAll();
    };

    return storage;
}
