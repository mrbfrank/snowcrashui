angular.module('storage', ['LocalStorageModule']);

angular
    .module('storage')
    .factory('StorageFactory', StorageFactory);
