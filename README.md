# Snow Crash UI #
Snow Crash UI is an AngularJS app built to manage game development settings in the cloud. Each setting is a key value pair, and can be assigned to groups. I've built it into the context of a game I wish to see made or perhaps make myself one day, based on the novel [Snow Crash](https://en.wikipedia.org/wiki/Snow_Crash) by Neal Stephenson.

### View the app ###
[https://snowcrashui.herokuapp.com](https://snowcrashui.herokuapp.com)

### API Documentation ###
[http://docs.snowcrash1.apiary.io](http://docs.snowcrash1.apiary.io)

### Build from source ###
Clone with Git & run the following commands in the project directory.
```
npm install
bower install
gulp
```
Then open in browser: [http://localhost:8085](http://localhost:8085)

### Notes ###
My app structure is based on John Papa's excellent [Angular Style Guide](https://github.com/johnpapa/angular-styleguide).

The settings module (/app/js/settings) contains html views for the settings editor, presentation logic, and a factory for retrieving settings and group objects from the API.

The mockserver module (/app/js/mockserver) is an interactive mock server data model with the necessary HTTP interceptors to fully emulate a functional API. Given that the mockserver is intercepting all API calls, you will not see them in the 'Network' tab of your browser's development tools, so I am logging all API requests and responses to the browser console instead. One benefit to this approach is that by simply omitting this module from a production deployment, the app will make real requests to the configured API url. Taking this concept further it would be simple to implement a switch in the app to engage an offline demo mode useful for development, training, or sales presentations.